  /invert

 const invert = obj => {
  const result = {};
  for (const e in obj) result[obj[e]] = e;

  return result;
};
console.log(invert({ first: 'allis', second: 'jake' }));


    /sorting  


const issorted = arr => {
  const result = [];
  for (let e of arr) {
    if (arr[e] < arr[e + 1]) return true;
  }
  return false;
};
console.log(issorted([1, 2, 3, 4, 5]));

  /anyofeven

const any = (f, arr) => {
  let result = false;
  for (const e of arr) result = result || f(e);
  return result;
};
const evn = e => {
  const result = false;

  if (e % 2 === 0) return true;

  return false;
};
console.log(any(evn, [2, 3, 5, 7]));
console.log(any(evn, [1, 3, 5]));

  /every even

const every = (f, arr) => {
  let result = true;
  for (const e of arr) result = result && f(e);
  return result;
};
const even = e => {
  const result = true;
  if (e % 2 === 0) return true;
  return false;
};
console.log(every(even, [2, 4, 6, 8]));
console.log(every(even, [1, 2, 3, 4, 5]));

  /every

const isevery = (f, arr) => {
  const result = [];
  for (const e of arr) if (e % 2 === 0) return true;
  return false;
};
console.log(every(even, [2, 3, 4, 5]));
console.log(every(even, [2, 4, 6, 8]));
console.log(every(even, [1, 3, 5]));

   /map,filter,reduce,reverse,take_while,drop_while

const even = x => x % 2 === 0;
const map = (f, arr) => {
  const result = [];
  for (const e of arr) result.push(f(e));
  return result;
};
console.log(map(even, [2, 4, 6]));

const evenn = x => x % 2 === 0;
const filter = (f, arr) => {
  const result = [];
  for (const e of arr) if (f(e)) result.push(e);
  return result;
};
console.log(filter(evenn, [1, 2, 3, 4, 5]));

const mul = (x, y) => x * y;
const reduce = (f, ini, arr) => {
  let result = ini;
  for (const e of arr) result = f(result, e);
  return result;
};
console.log(reduce(mul, 3, [1, 2, 3]));

const reverse = arr => {
  const result = [];
  for (let e = arr.length - 1; e >= 0; e--) result.push(arr[e]);

  return result;
};
console.log(reverse([1, 2, 3]));

const even = x => x % 2 === 0;
const take_while = (f, arr) => {
  const result = [];
  for (const e of arr)
    if (f(e)) result.push(e);
    else return result;
};
console.log(take_while(even, [2, 3, 4, 6]));

const even = x => x % 2 === 0;
const drop_while = (f, arr) => {
  const result = [];
  for (const e of arr)
    if (f(arr[e] === true))
      if (f(arr[e + 1] === false)) return arr.slice(e + 1);
};
console.log(drop_while(even, [2, 3, 4, 6, 5]));

