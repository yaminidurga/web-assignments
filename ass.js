const assoc = (k, v, obj) => {
  const result = clone(obj);
  result[k] = v;
  return result;
};
console.log(assoc("c", 3, { a: 1, b: 2 }));

const dissoc = (k, obj) => {
  const result = {};
  for (const i in obj) {
    if (i !== k) result[i] = obj[i];
  }
  return result;
};
console.log(dissoc("b", { a: 1, b: 2, c: 3 }));

