const clone = obj => {
  const result = {};
  for (let e in obj) {
    result[e] = obj[e];
  }
  return result;
};
console.log(clone({ x: 1, y: 2 }));

const merge = (obj1, obj2) => {
  const result = {};
  for (let e in obj1) result[e] = obj1[e];
  for (let e in obj2) result[e] = obj2[e];
  return result;
};
const rectangle = merge({ x: 1, y: 2 }, { width: 10, height: 20 });
console.log(rectangle);

