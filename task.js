
/repeat/

const repeat = (n, x) => {
  const result = [];
  for (let i = 0; i <= n; i++) {
    result.push(x);
  }
  return result;
};
console.log(repeat(1, "*"));

/square/

const square = arr => {
  const result = [];
  for (let i = 0; i <= arr.length; i++) result.push(arr[i] * arr[i]);
  return result;
};
console.log(square([2, 3]));
/power/

const power = (x, y) => {
  var result = 1;
  for (let i = 1; i <= y; i++) result = result * x;
  return result;
};
console.log(power(2, 3));
/indexof/

const indexof = (arr, v) => {
  for (let i = 0; i < arr.length; i++) if (arr[i] == v) return i;
};
console.log(indexof([1, 2, 3], 2));
/max/

const max = arr => {
  let max = 0;
  for (let i = 0; i < arr.length; i++) {
    arr[i] > max;
    max = arr[i];
  }
  return max;
};
console.log(max([1, 2, 3, 4, 5]));
/sum/

const sum = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum = sum + arr[i];
  }
  return sum;
};
console.log(sum([1, 2, 3]));

/factorial/

const factorial = n => {
  let fact = 1;
  for (let i = 1; i <= n; i++) {
    fact = fact * i;
  }
  return fact;
};
console.log(factorial(5));

/even/

const even = arr => {
  const result = [];
  for (let i = 0; i < arr.length; i++) {
    arr[i] % 2 == 0;
  }
  return result;
};
console.log(even([1, 2, 3]));

/take/

const take = (n, arr) => {
  const result = [];
  for (let i = 0; i < n; i++) {
    result.push(arr[i]);
  }
  return result;
};
console.log(take(5, [1, 2, 3, 4, 5, 6, 7, 8]));

/concat/

const concat = (arr1, arr2) => {
  const result = [];
  for (let i = 0; i < arr1.length; i++) {
    arr1.push(arr2[i]);
  }
  return result;
};
console.log(concat([1, 2, 3], [4, 5, 6]));

/prime/

const prime = n => {
  if (n >= 2) {
    for (let i = 2; i < n; i++) {
      if (n % i == 0) return false;
    }
    return true;
  }
  return false;
};
console.log(prime(5));

/reverse/

const reverse = arr => {
  const result = [];
  for (let i = arr.length - 1; i >= 0; i--) {
    result.push(arr[i]);
  }
  return result;
};
console.log(reverse([1, 2, 3]));

/drop/

const drop = (n, arr) => {
  const result = [];
  for (let i = n; i < arr.length; i++) {
    result.push(arr[i]);
  }
  return result;
};
console.log(drop(4, [1, 2, 3, 4, 5, 6, 7, 8]));


