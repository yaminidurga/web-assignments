   /interleave

const interleave = (arr, arr1) => {
  const result = [];
  for (const e in arr) {
    result.push(arr[e]);
    result.push(arr1[e]);
  }
  return result;
};

console.log(interleave([1, 2, 3], [4, 5, 6]));

   /interpose

const interpose = (arr, x) => {
  const result = [];
  for (const e in arr) result.push(arr[e], x);

  return result;
};

console.log(interpose([2, 3, 4], '/'));

